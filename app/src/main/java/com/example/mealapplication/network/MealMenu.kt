package com.example.mealapplication.network

import com.squareup.moshi.Json

data class MealMenu (
    @Json(name="strMeal")
    val strMeal: String,
    @Json(name="strMealThumb")
    val strMealThumb: String,
    @Json(name="idMeal")
    val idMeal: String
)

data class MealResponse(@Json(name="meals")
                        var result : List<MealMenu>)