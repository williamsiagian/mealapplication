package com.example.mealapplication.network

import com.squareup.moshi.Json

data class MealDetail (
    @Json(name="idMeal")
    val idMeal: String,
    @Json(name="strMeal")
    val strMeal: String,
    @Json(name="strDrinkAlternate")
    val strDrinkAlternate: String?=null,
    @Json(name="strCategory")
    val strCategory: String?=null,
    @Json(name="strArea")
    val strArea: String?=null,
    @Json(name="strInstructions")
    val strInstructions: String?=null,
    @Json(name="strMealThumb")
    val strMealThumb: String?=null,
    @Json(name="strTags")
    val strTags: String?=null,
    @Json(name="strYoutube")
    val strYoutube: String?=null,
    @Json(name="strSource")
    val strSource: String?=null,
    @Json(name="strImageSource")
    val strImageSource: String?=null,
    @Json(name="strCreativeCommonsConfirmed")
    val strCreativeCommonsConfirmed: String?=null,
    @Json(name="dateModified")
    val dateModified: String?=null,




    )

data class DetailResponse(@Json(name="meals")
                          var result : List<MealDetail>)