package com.example.mealapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mealapplication.adapter.MainAdapter
import com.example.mealapplication.network.MealMenu
import com.example.mealapplication.utils.ScreenState
import com.example.mealapplication.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.menuLiveData.observe(this, { state ->
            processMenuResponse(state)
        })
    }

    private fun processMenuResponse(state: ScreenState<List<MealMenu>?>){

        val pb =findViewById<ProgressBar>(R.id.progressBar)

        when(state){

            is ScreenState.Loading->{
                pb.visibility = View.VISIBLE
            }
            is ScreenState.Success->{
                pb.visibility = View.GONE
                if(state.data!=null){
                    val adapter = MainAdapter(state.data){
                        Log.d("testclick","main adapter $it")
                        val intent = Intent(this, DetailActivity::class.java)
                        intent.putExtra("mealId",it)
                        startActivity(intent)
                    }
                    val recyclerView = findViewById<RecyclerView>(R.id.rv_menu)
                    recyclerView?.layoutManager= LinearLayoutManager(this)
                    recyclerView.adapter=adapter
                }
            }
            is ScreenState.Error->{
                pb.visibility= View.GONE
                val view = pb.rootView
                Snackbar.make(view,state.message!!, Snackbar.LENGTH_SHORT).show()
            }
        }
    }
}