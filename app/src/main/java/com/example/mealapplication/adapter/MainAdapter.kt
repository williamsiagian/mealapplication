package com.example.mealapplication.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.example.mealapplication.R
import com.example.mealapplication.network.MealMenu

class MainAdapter(val mealMenuList: List<MealMenu>, val clickListener :(String)-> Unit ) : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    inner class MainViewHolder(private val itemView: View): RecyclerView.ViewHolder(itemView){

        fun bindData(_mealMenu: MealMenu){
            val name = itemView.findViewById<TextView>(R.id.tv_menu)
            val image = itemView.findViewById<ImageView>(R.id.iv_menu)

            name.text = _mealMenu.strMeal
            image.load(_mealMenu.strMealThumb){
                transformations(CircleCropTransformation())
            }

            itemView.rootView.setOnClickListener {
                clickListener.invoke(_mealMenu.idMeal)
                Log.d("testclick","di click")
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.menu_item, parent,false))
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bindData(mealMenuList[position])

    }

    override fun getItemCount(): Int {
        return mealMenuList.size
    }
}