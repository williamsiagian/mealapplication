package com.example.mealapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mealapplication.network.ApiClient
import com.example.mealapplication.network.DetailResponse
import com.example.mealapplication.network.MealDetail
import com.example.mealapplication.repository.MainRepository
import com.example.mealapplication.utils.ScreenState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel(private val repository: MainRepository = MainRepository(ApiClient.apiService)):
    ViewModel(){

    private var _detailLiveData = MutableLiveData<ScreenState<MealDetail>>()
    val detailLiveData: LiveData<ScreenState<MealDetail>>
        get() = _detailLiveData



     fun fetchDetail(id : String){


        viewModelScope.launch(Dispatchers.IO) {

            _detailLiveData.postValue(ScreenState.Loading(null))
            try {
                val mealDetail = repository.getDetail(id)
                _detailLiveData.postValue(ScreenState.Success(mealDetail))

            }catch (e : Exception){
                _detailLiveData.postValue(ScreenState.Error(e.message.toString(), null))
            }
        }

    }
}