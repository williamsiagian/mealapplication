package com.example.mealapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mealapplication.network.ApiClient
import com.example.mealapplication.network.MealMenu
import com.example.mealapplication.repository.MainRepository
import com.example.mealapplication.utils.ScreenState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(private val repository: MainRepository = MainRepository(ApiClient.apiService)):
    ViewModel(){

    private var _menuLiveData = MutableLiveData<ScreenState<List<MealMenu>?>>()
    val menuLiveData: LiveData<ScreenState<List<MealMenu>?>>
        get() = _menuLiveData

    init {
        fetchMenu()
    }

    private fun fetchMenu(){
        _menuLiveData.postValue(ScreenState.Loading(null))

        viewModelScope.launch(Dispatchers.IO) {
            try {
                val client = repository.getMenu("seafood")
                _menuLiveData.postValue(ScreenState.Success(client.result))

            }catch (e : Exception){
                _menuLiveData.postValue(ScreenState.Error(e.message.toString(), null))
            }
        }

    }
}