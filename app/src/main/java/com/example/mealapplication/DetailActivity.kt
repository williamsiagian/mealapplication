package com.example.mealapplication

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.example.mealapplication.adapter.MainAdapter
import com.example.mealapplication.utils.ScreenState
import com.example.mealapplication.viewmodel.DetailViewModel
import com.google.android.material.snackbar.Snackbar


class DetailActivity : AppCompatActivity() {

    private val viewModel: DetailViewModel by lazy {
        ViewModelProvider(this).get(DetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

    val nameDetail = findViewById<TextView>(R.id.detailName)
    val imageDetail = findViewById<ImageView>(R.id.detailImage)
    val categoryDetail = findViewById<TextView>(R.id.detailCategory)
    val tagsDetail = findViewById<TextView>(R.id.detailTags)
    val areaDetail = findViewById<TextView>(R.id.detailArea)
    val youtubeButton = findViewById<Button>(R.id.buttonYoutube)
    val sourceButton = findViewById<Button>(R.id.buttonSource)

    val mealMenuId = intent.getStringExtra("mealId").toString()
        Log.d("testclick","$mealMenuId")

    viewModel.fetchDetail(mealMenuId)

        viewModel.detailLiveData.observe(this, { state ->
            when(state){

                is ScreenState.Loading->{

                }
                is ScreenState.Success->{

                    if(state.data!=null){
                      nameDetail.text=state.data.strMeal

                      imageDetail.load(state.data.strMealThumb){
                          transformations(CircleCropTransformation())
                      }

                      categoryDetail.text= state.data.strCategory
                      tagsDetail.text=state.data.strTags
                      areaDetail.text=state.data.strArea

                      youtubeButton.setOnClickListener {
                          val linkYoutube = state.data.strYoutube
                          val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(linkYoutube))
                          startActivity(browserIntent)
                      }

                      sourceButton.setOnClickListener {
                          val linkSource = state.data.strSource
                          val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(linkSource))
                          startActivity(browserIntent)
                      }

                    }
                }
                is ScreenState.Error->{

                        Log.d("testclick","ini error")
                }
            }
        })
    }


}
