package com.example.mealapplication.repository

import com.example.mealapplication.network.ApiService
import com.example.mealapplication.network.DetailResponse
import com.example.mealapplication.network.MealDetail
import com.example.mealapplication.network.MealResponse

class MainRepository(private val apiService: ApiService) {
    suspend fun getMenu(c:String): MealResponse = apiService.fetchMeals(c)
    suspend fun getDetail(i:String): MealDetail {
        val result = apiService.fetchDetails(i).result

        return result.first()
    }
}